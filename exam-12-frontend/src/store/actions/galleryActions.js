import axios from '../../axios-api';
import {NotificationManager} from "react-notifications";

export const FETCH_GALLERIES_SUCCESS = 'FETCH_GALLERIES_SUCCESS';
export const CREATE_GALLERY_SUCCESS = 'CREATE_GALLERY_SUCCESS';

export const fetchGalleriesSuccess = galleries => ({type: FETCH_GALLERIES_SUCCESS, galleries});
export const createGalleriesSuccess = () => ({type: CREATE_GALLERY_SUCCESS});

export const fetchGallery = (userId) => {
  return dispatch => {
    let url = '/galleries';
    if (userId) {
      url += '?user=' + userId
    }
    return axios.get(url).then(
      response => dispatch(fetchGalleriesSuccess(response.data))
    );
  };
};

export const createGalley = galleryData => {
  return dispatch => {
    return axios.post('/galleries', galleryData).then(
      () => {
        dispatch(createGalleriesSuccess());
        NotificationManager.success('You added new photo!');
      }
    );
  };
};

export const removedGallery = (id, remove) => {
  return dispatch => {
    return axios.post(`/galleries/${id}/toggle_removed`, remove).then(
      () => {
        dispatch(fetchGallery());
        NotificationManager.warning('You removed an photo!');
      }
    );
  };
};

