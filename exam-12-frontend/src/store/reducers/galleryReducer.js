import {FETCH_GALLERIES_SUCCESS} from "../actions/galleryActions";

const initialState = {
  gallery: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_GALLERIES_SUCCESS:
      return {...state, gallery: action.galleries};
    default:
      return state;
  }
};

export default reducer;