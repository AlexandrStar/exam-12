import React, {Component, Fragment} from 'react';
import {connect} from "react-redux";

import PhotoForm from "../../components/PhotoForm/PhotoForm";
import {createGalley} from "../../store/actions/galleryActions";

class NewPhoto extends Component {
  createPhoto = galleryData => {
    this.props.createGalley(galleryData).then(() => {
      this.props.history.push('/');
    });
  };

  render() {
    return (
      <Fragment>
        <h2>New photo</h2>
        <PhotoForm
          user={this.props.user}
          onSubmit={this.createPhoto}
        />
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  createGalley: galleryData => dispatch(createGalley(galleryData))
});

export default connect(mapStateToProps, mapDispatchToProps)(NewPhoto);
