import React, {Component, Fragment} from 'react';
import {fetchGallery, removedGallery} from "../../store/actions/galleryActions";
import {connect} from "react-redux";
import {
  Button,
  Card,
  CardBody,
  CardImg,
  CardText,
  CardTitle,
  Col,
  Modal,
  ModalBody,
  ModalFooter,
  Row
} from "reactstrap";
import {apiURL} from "../../constans";
import {Link} from "react-router-dom";

class Gallery extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modal: false
    };

    this.toggle = this.toggle.bind(this);
  }

  toggle() {
    this.setState(prevState => ({
      modal: !prevState.modal
    }));
  }

  componentDidMount() {
    this.props.onFetchPhoto(this.props.match.params.userId)
  };

  componentDidUpdate(prevProps) {
    if (prevProps.match.params.userId !== this.props.match.params.userId) {
      this.props.onFetchPhoto(this.props.match.params.userId);
    }
  };

  render() {
    return (
      <Fragment>
        <Row>
          <Col sm={12}>
              {this.props.user ?
                <Link to="/galleries/new">
                  <Button
                    style={{marginBottom: '20px'}}
                    color="primary"
                    className="float-right"
                  >
                    Add product
                  </Button>
                </Link>
                : null}
          </Col>
          {this.props.photo.map(photo => {
            return (
            <Col key={photo._id} sm={2} style={{marginBottom: '25px'}}>
              <Card style={{height: '100%'}}>
                <CardImg top style={{width: "100%"}} src={apiURL + '/uploads/' + photo.photo} alt="Gallery"
                         onClick={this.toggle}
                />
                <CardBody>
                  <CardTitle>{photo.title}</CardTitle>

                  <CardText>By:
                    <Link to={`/galleries/${photo.user._id}`}>
                      {photo.user.displayName}
                    </Link>
                  </CardText>
                </CardBody>
                {this.props.user && this.props.user._id === photo.user._id ?
                  <Col sm={{offset: 6, size: 2}}>
                    <Button
                      style={{margin: '5px'}}
                      color="danger"
                      className="float-right"
                      onClick={() => this.props.removePhoto(photo._id, {removed: true})}
                    >
                      Remove
                    </Button>
                  </Col> : null
                }
              </Card>

              <Modal isOpen={this.state.modal} toggle={this.toggle}>
                <ModalBody>
                  <img style={{width: '100%'}} src={apiURL + '/uploads/' + photo.photo} alt="Gallery"/>
                </ModalBody>
                <ModalFooter>
                  <Button color="secondary" onClick={this.toggle}>Close</Button>
                </ModalFooter>
              </Modal>

            </Col>
            )})}
        </Row>
      </Fragment>
    );
  }
}

const mapStateToProps = state => ({
  photo: state.gallery.gallery,
  user: state.users.user
});

const mapDispatchToProps = dispatch => ({
  onFetchPhoto: (userId) => dispatch(fetchGallery(userId)),
  removePhoto: (id, remove) => dispatch(removedGallery(id, remove))
});

export default connect(mapStateToProps, mapDispatchToProps)(Gallery);