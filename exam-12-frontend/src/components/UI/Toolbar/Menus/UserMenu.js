import React, {Fragment} from 'react';
import {Button, NavItem} from "reactstrap";
import {Link} from "react-router-dom";

const UserMenu = ({user, logout}) => {
  const BASE_URL = 'http://localhost:8000/uploads/';
  const src = user.facebookId? user.avatarImage : BASE_URL + user.avatarImage;
  return (
    <Fragment>
      <NavItem>
        <img width='40px' src={src} alt="userImg"/>
        Hello,
        <Link to={`/galleries/${user._id}`}> {user.displayName}!</Link>
      </NavItem>
      <NavItem>
        <Button style={{marginLeft: '10px'}} onClick={logout}>Logout</Button>
      </NavItem>
    </Fragment>
  );
};

export default UserMenu;
