const express = require('express');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const config = require('../config');

const auth = require('../middleware/auth');
const addAuth = require('../middleware/addAuth');
const Gallery = require('../models/Gallery');
const User = require('../models/User');

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, config.uploadPath);
  },
  filename: (req, file, cb) => {
    cb(null, nanoid() + path.extname(file.originalname));
  }
});

const upload = multer({storage});

const router = express.Router();

router.get('/', addAuth, async (req, res) => {
  try {
    let criteria = {removed: false};

    if (req.query.user) {
      criteria.user = req.query.user;
    }

    const gallery = await Gallery.find(criteria).populate('user', '_id, displayName');

    return res.send(gallery)
  }catch (e) {
    return res.status(500).send(e)
  }

});

router.post('/', auth, upload.single('photo'), (req, res) => {
  const galleryData = req.body;

  if (req.file) {
    galleryData.photo = req.file.filename;
  }

  galleryData.user = req.user._id;

  const gallery = new Gallery(galleryData);

  gallery.save()
    .then(result => res.send(result))
    .catch(error => res.status(400).send(error));
});

router.post('/:id/toggle_removed', auth, async (req, res) => {
  const gallery = await Gallery.findById(req.params.id);

  if (!gallery) {
    return res.sendStatus(404);
  }

  gallery.removed = !gallery.removed;

  await gallery.save();

  res.send(gallery);
});

module.exports = router;