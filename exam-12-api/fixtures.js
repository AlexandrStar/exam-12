const mongoose = require('mongoose');
const config = require('./config');
const nanoid = require('nanoid');

const User = require('./models/User');
const Gallery = require('./models/Gallery');

const run = async () => {
  await mongoose.connect(config.dbUrl, config.mongoOptions);

  const connection = mongoose.connection;

  const collections = await connection.db.collections();

  for (let collection of collections) {
    await collection.drop();
  }

  const user = await User.create(
    {
      username: 'user',
      password: '123',
      token: nanoid(),
      displayName: 'Anonymous',
      avatarImage:'noavatar.jpg',
    },
    {
      username: 'john',
      password: '123',
      token: nanoid(),
      displayName: 'John Doe',
      avatarImage:'admin.jpg',
    },
  );

  await Gallery.create(
    {
      user: user[0]._id,
      title: 'Король и Шут',
      photo: 'korol-i-shut.jpeg',
    },
    {
      user: user[0]._id,
      title: 'ACDC',
      photo: 'ACDC.jpg',
    },
    {
      user: user[0]._id,
      title: 'Rammstein',
      photo: 'rammstein.jpg',
    },
    {
      user: user[1]._id,
      title: 'Highway to Hell',
      photo: 'acdc.png',
    },
    {
      user: user[1]._id,
      title: 'Как в старой сказке',
      photo: 'skazka.jpg',
    },
    {
      user: user[1]._id,
      title: 'Mutter',
      photo: 'mutter.jpeg',
    },
  );

  await connection.close();
};

run().catch(error => {
  console.log('Something went wrong', error);
});