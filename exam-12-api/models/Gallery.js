const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const GallerySchema = new Schema({
  user: {
    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true,
  },
  title: {
    type: String,
    require: true,
  },
  photo: {
    type: String,
    required: true,
  },
  removed: {
    type: Boolean,
    required: true,
    default: false
  },
});

const Gallery = mongoose.model('Gallery', GallerySchema);

module.exports = Gallery;